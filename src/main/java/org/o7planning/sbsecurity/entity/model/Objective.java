package org.o7planning.sbsecurity.entity.model;


import java.util.ArrayList;

public class Objective extends Question{

	public Objective(String type, int qid, String question, int secId,ArrayList<String> answers, String userAnswer, String actualAnswer) {
	super(type, qid, question, secId,answers, userAnswer, actualAnswer); 
	}

}
