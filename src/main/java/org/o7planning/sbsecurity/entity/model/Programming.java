package org.o7planning.sbsecurity.entity.model;

import java.util.ArrayList;

public class Programming extends Question{
	public Programming(String type, int qId, String question, int secId,String input, String output, String testCasePasses,String code) {
	super(type, qId, question,secId, input, output, testCasePasses,code);
	}
}