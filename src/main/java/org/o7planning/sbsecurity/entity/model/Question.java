package org.o7planning.sbsecurity.entity.model;

import java.util.ArrayList;

public class Question {
	int qId;
	String qType;
	
	String section_name;
	
	public String getSection_name() {
		return section_name;
	}

	public void setSection_name(String section_name) {
		this.section_name = section_name;
	}

	public String getqType() {
		return qType;
	}

	public void setqType(String qType) {
		this.qType = qType;
	}

	public int getSection_id() {
		return section_id;
	}

	public void setSection_id(int section_id) {
		this.section_id = section_id;
	}

	public String getOp1() {
		return op1;
	}

	public void setOp1(String op1) {
		this.op1 = op1;
	}

	public String getOp2() {
		return op2;
	}

	public void setOp2(String op2) {
		this.op2 = op2;
	}

	public String getOp3() {
		return op3;
	}

	public void setOp3(String op3) {
		this.op3 = op3;
	}

	public String getOp4() {
		return op4;
	}

	public void setOp4(String op4) {
		this.op4 = op4;
	}

	public int getAop() {
		return aop;
	}

	public void setAop(int aop) {
		this.aop = aop;
	}

	String question;
	int section_id;
	ArrayList<String> answers=new ArrayList<String>();
	String op1, op2, op3, op4;
	int aop;
	String userAnswer;
	String actualAnswer;
	String input;
	String output;
	String testCasePasses;
	String code;


	public String getType() {
	return qType;
	}

	public void setType(String type) {
	this.qType = type;
	}

	public String getQuestion() {
	return question;
	}

	public void setQuestion(String question) {
	this.question = question;
	}

	public int getSecId() {
	return section_id;
	}

	public void setSecId(int secId) {
	this.section_id = secId;
	}

	public ArrayList<String> getAnswers() {
	return answers;
	}

	public void setAnswers(ArrayList<String> answers) {
	this.answers = answers;
	}

	public String getUserAnswer() {
	return userAnswer;
	}

	public void setUserAnswer(String userAnswer) {
	this.userAnswer = userAnswer;
	}

	public String getActualAnswer() {
	return actualAnswer;
	}

	public void setActualAnswer(String actualAnswer) {
	this.actualAnswer = actualAnswer;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getTestCasePasses() {
	return testCasePasses;
	}

	public void setTestCasePasses(String testCasePasses) {
	this.testCasePasses = testCasePasses;
	}

	public String getCode() {
	return code;
	}

	public void setCode(String code) {
	this.code = code;
	}

	public int getqId() {
	return qId;
	}

	public void setqId(int qId) {
	this.qId = qId;
	}
	
	public Question(String type, int qid, String question,int secId,String input, String output, String testCasePasses,String code) {
		this.qId=qid;
		this.qType = type;
		this.question = question;
		this.section_id=secId;
		this.input = input;
		this.output = output;
		this.testCasePasses = testCasePasses;
		this.code=code;
		}

		//objective
		public Question(String type, int qid, String question,int secId, ArrayList<String> answers, String userAnswer, String actualAnswer) {
		super();
		this.qId=qid;
		this.qType = type;
		this.question = question;
		this.section_id=secId;
		this.op1=answers.get(0);
		this.op2=answers.get(1);
		this.op3=answers.get(2);
		this.op4=answers.get(3);
		this.userAnswer = userAnswer;
		this.actualAnswer=actualAnswer;
		}

		//subjective
		public Question(String type, int qid, String question,int secId, String userAnswer) {
		this.qId=qid;
		this.qType = type;
		this.question = question;
		this.section_id=secId;
		this.userAnswer = userAnswer;
		}

		@Override
		public String toString() {
			return "Question [qId=" + qId + ", qType=" + qType + ", question=" + question + ", section_id=" + section_id
					+ ", answers=" + answers + ", op1=" + op1 + ", op2=" + op2 + ", op3=" + op3 + ", op4=" + op4
					+ ", aop=" + aop + ", userAnswer=" + userAnswer + ", actualAnswer=" + actualAnswer + ", input="
					+ input + ", output=" + output + ", testCasePasses=" + testCasePasses + ", code=" + code + "]";
		}

		public Question() {
		// TODO Auto-generated constructor stub
		} 	
}
