package org.o7planning.sbsecurity.entity.model;

public class Input {
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
