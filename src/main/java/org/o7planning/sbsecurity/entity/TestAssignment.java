package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Assigned_test")

@NamedQueries({ 
@NamedQuery(name = "Test.findDTO", query = "Select t from TestAssignment t where t.username=:username"),
@NamedQuery(name = "Test.findALL", query = "Select t from TestAssignment t"),
@NamedQuery(name = "Test.findOne", query = "Select t from TestAssignment t where t.tokenId=:tokenId"),
@NamedQuery(name = "Test.endTest", query = "Update TestAssignment t set score=:score where t.tokenId=:tokenId")

})
public class TestAssignment implements Serializable{
	
	public TestAssignment(String tokenId, Date expiry_date, int userid, String testType, String username) {
		super();
		this.tokenId = tokenId;
		this.expiry_date = expiry_date;
		this.userid = userid;
		this.testType = testType;
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Id
    @Column(name = "TEST_TOKENID",  nullable = false)
	String tokenId;
	 @Column(name = "STATUS",  nullable = false)
	String status;
	 @Column(name = "SCORE",  nullable = false)
	 int score;
	 
    public TestAssignment(String tokenId, String status, int score, Date expiry_date, int userid, String testType,
			String username) {
		super();
		this.tokenId = tokenId;
		this.status = status;
		this.score = score;
		this.expiry_date = expiry_date;
		this.userid = userid;
		this.testType = testType;
		this.username = username;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public TestAssignment(String tokenId, String status, Date expiry_date, int userid, String testType,
			String username) {
		super();
		this.tokenId = tokenId;
		this.status = status;
		this.expiry_date = expiry_date;
		this.userid = userid;
		this.testType = testType;
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "EXPIRY_DATE",  nullable = false)
	Date expiry_date;
	public TestAssignment() {
		
	}
	
	public TestAssignment(String tokenId, Date expiry_date, int userid, String testType) {
		super();
		this.tokenId = tokenId;
		this.expiry_date = expiry_date;
		this.userid = userid;
		this.testType = testType;
	}

	@Column(name="USERID")
	int userid;
	
    public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	@Override
	public String toString() {
		return "TestAssignment [tokenId=" + tokenId + ", expiry_date=" + expiry_date + ", userid=" + userid
				+ ", testType=" + testType + ", username=" + username + "]";
	}

	public TestAssignment(String tokenId, Date expiry_date, String testType) {
		super();
		this.tokenId = tokenId;
		this.expiry_date = expiry_date;
		this.testType = testType;
	}
	public Date getExpiry_date() {
		return expiry_date;
	}
	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}
	public TestAssignment(String tokenId, String testType) {
		super();
		this.tokenId = tokenId;
		this.testType = testType;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	@Column(name = "TEST_TYPE", nullable = false)
	String testType;
	

	@Column(name = "USERNAME", nullable = false)
	String username;
}
