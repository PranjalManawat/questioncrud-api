package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.TestAssignment;
import org.o7planning.sbsecurity.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TestAssignmentDAO {
	  @Autowired
	    private EntityManager entityManager;
	 
	    public List<TestAssignment> getTests(String username) {
	        String sql = "Select tokenId,testType from TestAssignment";
	 
	        return entityManager.createNamedQuery("Test.findDTO", TestAssignment.class).setParameter("username", username).getResultList();
	        //return (List<TestAssignment>)query.getResultList();
	    }
	    

	    public List<TestAssignment> getAllTests() {
	        String sql = "Select tokenId,testType from TestAssignment";
	 
	        return entityManager.createNamedQuery("Test.findALL", TestAssignment.class).getResultList();
	        //return (List<TestAssignment>)query.getResultList();
	    }
	    
	    public TestAssignment getTest(String testId) {
	        return entityManager.createNamedQuery("Test.findOne", TestAssignment.class).setParameter("tokenId",testId).getSingleResult();
	    }
	    
	    public boolean endTest(String testId,int score) {
	        entityManager.createNamedQuery("Test.endTest", TestAssignment.class).setParameter("tokenId",testId).setParameter("score",score).getSingleResult();
	    	return true;
	    }
}
