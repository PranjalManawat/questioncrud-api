package org.o7planning.sbsecurity.dao;

import java.util.ArrayList;
import java.util.List;

import org.o7planning.sbsecurity.dao.mapper.studentmapperObjective;
import org.o7planning.sbsecurity.dao.mapper.studentmapperProgramming;
import org.o7planning.sbsecurity.dao.mapper.studentmapperSubjective;
import org.o7planning.sbsecurity.entity.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class QuestionDAO implements QuestionCRUD {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Question> getAllQuestions(String type) {
		List<Question> questionsList = new ArrayList();
		String sql;
		if (type.equalsIgnoreCase("subjective") || type.equalsIgnoreCase("all")) {
			sql = "select * from subjective";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperSubjective()));
		}

		if (type.equalsIgnoreCase("objective") || type.equalsIgnoreCase("all")) {
			sql = "select * from Objective";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperObjective()));
		}

		if (type.equalsIgnoreCase("programming") || type.equalsIgnoreCase("all")) {
			sql = "select * from programming";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperProgramming()));
		}

		if (questionsList.size() < 1)
			return null;

		return questionsList;
	}

	public int addQuestion(Question question) {

		String sql;

		if (question == null)
			return -1;

		if (question.getType().equalsIgnoreCase("subjective")) {
			System.out.println(question);
			sql = "SELECT max(S_ID) AS ID FROM SUBJECTIVE";
			int Max_S_ID = (int) jdbcTemplate.queryForObject(sql, int.class);
			System.out.println("this is max id" + Max_S_ID);
			sql = "INSERT INTO SUBJECTIVE VALUES(?, ?, ?)";
			return jdbcTemplate.update(sql, new Object[] { Max_S_ID + 1, question.getQuestion(), question.getSecId() });// other
																														// fields
		} else if (question.getType().equalsIgnoreCase("objective")) {
			sql = "SELECT max(OB_ID) AS ID FROM OBJECTIVE";
			int Max_O_ID = (int) jdbcTemplate.queryForObject(sql, new Object[] {}, int.class);
			sql = "INSERT INTO OBJECTIVE VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
			return jdbcTemplate.update(sql, new Object[] { Max_O_ID + 1, question.getQuestion(), question.getOp1(),
					question.getOp2(), question.getOp3(), question.getOp4(), question.getAop(), question.getSecId() });
		} else if (question.getType().equalsIgnoreCase("programming")) {
			sql = "SELECT max(P_ID) AS ID FROM PROGRAMMING";
			int Max_P_ID = (int) jdbcTemplate.queryForObject(sql, new Object[] {}, int.class);
			sql = "INSERT INTO PROGRAMMING VALUES(?, ?, ?, ?, ?)";
			System.out.println("this is test " + Max_P_ID);
			return jdbcTemplate.update(sql, new Object[] { Max_P_ID + 1, question.getQuestion(), question.getInput(),
					question.getOutput(), question.getSecId() });
		}

		return 0;
	}

	public Question getQuestion(int qid, String type) {
		List<Question> question = new ArrayList<Question>(1);
		String sql;
		if (type.equalsIgnoreCase("subjective")) {
			sql = "select * from subjective where S_ID=?";
			question = jdbcTemplate.query(sql, new Object[] { qid }, new studentmapperSubjective());
		} else if (type.equalsIgnoreCase("objective")) {
			sql = "select * from Objective where OB_ID=?";
			question = jdbcTemplate.query(sql, new Object[] { qid }, new studentmapperObjective());
		} else if (type.equalsIgnoreCase("programming")) {
			sql = "select * from programming where P_ID=?";
			question = jdbcTemplate.query(sql, new Object[] { qid }, new studentmapperProgramming());
		} else
			return null;

		if (question.size() < 1)
			return null;

		return question.get(0);
	}

	public int modifyQuestion(Question question) {
		String sql;
		if (question.getType().equalsIgnoreCase("subjective")) {
			sql = "UPDATE SUBJECTIVE SET  QUESTION = ?, SEC_ID = ? WHERE S_ID = ?";
			return jdbcTemplate.update(sql, new Object[] {question.getQuestion(), question.getSecId(), question.getqId() });// other
																														// fields
		} else if (question.getType().equalsIgnoreCase("objective")) {
			sql = "UPDATE OBJECTIVE SET  QUESTION = ?, OP1 = ?, OP2 = ?, OP3 = ?, OP4 = ?, AOP = ?, SEC_ID = ? WHERE OB_ID = ?";
			return jdbcTemplate.update(sql, new Object[] {  question.getQuestion(), question.getOp1(),
					question.getOp2(), question.getOp3(), question.getOp4(), question.getAop(), question.getSecId(), question.getqId() });
		} else if (question.getType().equalsIgnoreCase("programming")) {
			sql = "UPDATE PROGRAMMING SET  P_STMT = ?, INPUT = ?, OUTPUT = ?, SEC_ID = ? WHERE P_ID = ?";
			return jdbcTemplate.update(sql, new Object[] {  question.getQuestion(), question.getInput(),
					question.getOutput(), question.getSecId(), question.getqId() });
		}
		return 0;
	}
}
