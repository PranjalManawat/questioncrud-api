package org.o7planning.sbsecurity.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.o7planning.sbsecurity.entity.model.Subjective;
import org.springframework.jdbc.core.RowMapper;



public class studentmapperSubjective implements RowMapper {

	public Subjective mapRow(ResultSet rs, int rowNum) throws SQLException {

	Subjective s=new Subjective("subjective",rs.getInt("S_ID"),rs.getString("QUESTION"),rs.getInt("SEC_ID"),"");
	return s;
	}

}
