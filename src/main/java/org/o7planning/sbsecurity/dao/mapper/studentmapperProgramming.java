package org.o7planning.sbsecurity.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import org.o7planning.sbsecurity.entity.model.Programming;

public class studentmapperProgramming implements RowMapper {

	public Programming mapRow(ResultSet rs, int rowNum) throws SQLException {

	String input=rs.getString("INPUT");
	String output=rs.getString("OUTPUT");

	Programming p=new Programming("coding",rs.getInt("P_ID"),rs.getString("P_STMT"),rs.getInt("SEC_ID"),input,output,"","");
	return p;
	}

}
