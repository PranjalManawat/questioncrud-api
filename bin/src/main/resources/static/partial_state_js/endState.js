function endTestState(questions,time,token){
	var url="http://localhost:8900/completeTest/"+token;
	var data={
	    "tokenId": token,
	    "timeLeft": time,
	    "state": questions
	};
	console.log("provide data",data);
	 $.ajax({
		 url:url,
		 dataType: 'json',
		 type: 'post',
		 contentType: 'application/json',
		 data: JSON.stringify(data),
		 success:function(data,status){
			 console.log("End State",data);
			 return true;
		 },
		 error:function(error,status){
			 console.log(error);
			 return false;
		 }
	 });
	 
}

