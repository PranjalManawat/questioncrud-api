/* function openFullscreen(elem) {
    		  elem = elem || document.documentElement;
    		  if (
    		    !document.fullscreenElement &&
    		    !document.mozFullScreenElement &&
    		    !document.webkitFullscreenElement &&
    		    !document.msFullscreenElement
    		  ) {
    		    if (elem.requestFullscreen) {
    		      elem.requestFullscreen();
    		    } else if (elem.msRequestFullscreen) {
    		      elem.msRequestFullscreen();
    		    } else if (elem.mozRequestFullScreen) {
    		      elem.mozRequestFullScreen();
    		    } else if (elem.webkitRequestFullscreen) {
    		      elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    		    }
    }
 }
  */ 

//updates the current question the screen
function changeQuestion(value) {
	var idx=value;
	if(idx<10){
		idx='0'+idx;
	}
	idx='#q_'+idx;
	$(idx).removeClass("question_select_box");
	$(idx).addClass("question_select_box_show_a");
	//alert("q_"+idx);
    //document.getElementById("q_"+idx).innerHtml+"Hello";
    currentQuestion = value;
    console.log("CUrrent question", currentQuestion,idx);
    setUpScreen(parseInt(value));
}
//handles the state and questions and answer on the screen

function viewVisibility(id,toggle){
    switch(toggle){
        case "hide":
                $(id).hide();
                break;
        case "show":
                $(id).show();
                break;      
    }
}
function setUpScreen(value) {

    if(value==1){
        viewVisibility("#previousQuestion","hide");
    }
    else{
        viewVisibility("#previousQuestion","show");
    }

    if(value==questionPaper.length){
        viewVisibility("#nextQuestion","hide");
    }
    else{
        viewVisibility("#nextQuestion","show");
    }

    $("#question_count").text(`Question ${value}`);
    $('#q').text(questionPaper[value - 1].question);
    if (questionPaper[value - 1].qType == "objective") {

        $('#o1').text(questionPaper[value - 1].answers[0]);
        $('#o2').text(questionPaper[value - 1].answers[1]);
        $('#o3').text(questionPaper[value - 1].answers[2]);
        $('#o4').text(questionPaper[value - 1].answers[3]);
        toggleOptionsBasedOnType("objective");
        if (questionPaper[value - 1].userAnswer != "") {
            answerSelected(parseInt(questionPaper[value - 1].userAnswer));
        }
        else {
            clearAll();
        }
    }
    else if (questionPaper[value - 1].qType == "subjective") {
        toggleOptionsBasedOnType("subjective");
        $('#testarea').val("");
        $('#testarea').val(questionPaper[value - 1].userAnswer);
        console.log(questionPaper[value - 1].userAnswer);
    }
    else if(questionPaper[value - 1].qType == "coding"){
        toggleOptionsBasedOnType("coding");
        $('#code_area').val("");
        console.log(questionPaper[value - 1]);
        $('#code_area').val(questionPaper[value - 1].code);
        $('#test_input').val(questionPaper[value - 1].input.join("\n"));
        $('#test_output').val(questionPaper[value - 1].output.join("\n"));
        $('#console_compiler').text();
    }
}

//toggle controls for questions based on the question type
//toToggle parameter is boolean value
//if true then subjective else objective
function toggleOptionsBasedOnType(toToggle) {
    //toToggle when to show subjective
    switch(toToggle){
        case "subjective":
        $('#o1').hide();
        $('#o2').hide();
        $('#o3').hide();
        $('#o4').hide();
        $('#testarea').show();
        $('#save_subjective').show();
        $('#coding_screen').hide();
        //$('#clearQuestion').hide();
        break;
        case "objective":
        $('#o1').show();
        $('#o2').show();
        $('#o3').show();
        $('#o4').show();
        //$('#clearQuestion').show();

        $('#testarea').hide();
        $('#save_subjective').hide();
        $('#coding_screen').hide();

        break;
        case "coding":
                $('#o1').hide();
                $('#o2').hide();
                $('#o3').hide();
                $('#o4').hide(); 
                $('#testarea').hide();
        $('#save_subjective').hide();
        $('#coding_screen').show();
        $('#console_compiler').text("");
       // $('#clearQuestion').hide();

        break;
    }
}


//get the final test score
function getScore() {
    var score = 0;
    console.log(questionPaper);
    for (var i = 0; i < questionPaper.length; i++) {
        if (questionPaper[i].qType == "objective") {
            if (questionPaper[i].userAnswer == questionPaper[i].actualAnswer) {
                score++;
            } else {
                //alert("Wrong Answer");
            }
        }
    }
    return score;
}

//function will update the ui when answer is selected
function answerSelected(num) {
    clearAll();
    $("#o" + num).css("background-color", "#7bda6f");
    $("#o" + num).css("color", "green");
    questionPaper[currentQuestion - 1].userAnswer = `${num}`;
    updateInitState(questionPaper[currentQuestion - 1],testTimer,testID,questionPaper[currentQuestion - 1].qType);
    updateNavButtons("selected")
}

//function which will clear all the selected option the screen
function clearAll() {
    for (var i = 1; i <= 4; i++) {
        $("#o" + i).css("background-color", "white");
        $("#o" + i).css("color", "black");
    }
    questionPaper[currentQuestion - 1].userAnswer = "";
    updateNavButtons("unselected")
}

function updateNavButtons(toggle){
	 var idx=currentQuestion;
		if(idx<10){
			idx='0'+idx;
		}
		idx='#q_'+idx;
		switch(toggle){
		case "selected":
			$(idx).removeClass("question_select_box");
			$(idx).addClass("question_select_box_show_a");
		
		break;
		case "unselected":
			$(idx).removeClass("question_select_box_show_a");
			$(idx).addClass("question_select_box");
			break;
		}
}