
//function which shows the modal on click of submit or timeout
function showExitModal(type) {
    //case 1 timeout
    //case 0 userdefined
    var modalTitle = "";
    var modalBody = "";
    switch (type) {
        case 1:
            modalBody = "Time out, Your time is over , click on the proceed button and exit the test\n\nDon't Close the window directly";
            modalTitle = "Your Time is up";
            $('#modal_close_button').hide();
            $('#modal_close').hide();
            isTimeOut = true;
            break;
        case 0:
            modalTitle = "Do You want to exists the test";
            modalBody = "kindly note, once you select proceed , you cannot give the test again"
            $('#modal_close_button').show();
            $('#modal_close').show();

            break;
    }
    $('#modal_title').text(modalTitle);
    $('#modal_body').text(modalBody);
    
    $('#showModal').click();

}


