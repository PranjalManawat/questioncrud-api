$(document).ready(function () {
    $('#showModal').hide();
    $('#alert_saved').hide();
    $('#total_questions').text(`Total Questions ${questionPaper.length}`);

    //side button navigation creation logic
    for (var i = 1; i <= questionPaper.length; i++) {
        var value = i;
        if (value < 10) {
            value = '0' + value;
        }
        $('#qid_col').append(`<button class="btn shadow question_select_box " id="q_${value}"
       onclick="changeQuestion(${value})">
        ${value}
    </button>`);
    }

    //setuo initial configuration for the screen
    setUpScreen(currentQuestion);
    display = $('#timer');
    startTimer(testTimer, display);

    //next button action listner
    $('#nextQuestion').on('click', function () {
        if (currentQuestion < questionPaper.length) {
            currentQuestion++;
            setUpScreen(currentQuestion);
        }
        else {

        }
    });

    //previous button action listners
    $('#previousQuestion').on('click', function () {
        if (currentQuestion > 1) {
            currentQuestion--;
            setUpScreen(currentQuestion);
        }
        else {

        }
    });

    //show modal when user click on the submit button
    $('#submitTest').on('click', function () {
        showExitModal(0);
    });

    //modal proceed click action listener
    $('#modal_proceed').on('click', function () {
        alert("Score is" + getScore());
        alert("redirect to the last page");
    });

    $('#save_subjective').on('click', function () {
        var alerTimer = 3;
        //save the user response in the json
        if($('#testarea').val()!=""){
            questionPaper[currentQuestion - 1].userAnswer = $('#testarea').val();
            $('#alert_saved').addClass("alert-success");
            $('#alert_saved').removeClass("alert-danger");
            $('#alert_saved').text("Answer successfully saved for Question no " + (currentQuestion)+"");
        }
        else{
            $('#alert_saved').removeClass("alert-success");
            $('#alert_saved').addClass("alert-danger");

            $('#alert_saved').text("Empty answers cannot be saved");

        }

        $('#alert_saved').show();
        //start timer for the alert to disappear
        var closeTimer = setInterval(function () {
            alerTimer--;
            if (alerTimer == 0) {
                $('#alert_saved').hide();
                clearInterval(closeTimer);
            }

        }, 1000);


    });

    //disable escape key
    $(document).on('keyup', function (evt) {
        if (evt.keyCode == 27) {
            if (isTimeOut) {
                alert('Will directly exist the test');
            }
            else {
                alert("escape key allowed");
            }
        }
        if (evt.keyCode == 116 || evt.keyCode == 82) {
           /* alert("Refresh not allowed");
            evt.preventDefault();*/
        }
    });

    //keydown event handling
    $(document).on('keydown', function (evt) {
        console.log("key down pressed " + evt.keyCode);
        if (evt.keyCode == 116) {
            evt.preventDefault();
            console.log("prevented default");
        }
    });
});