
var isTimeOut=false;
  //function which handles the timers
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var iiid = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        //console.log(timer);
        display.text(minutes + ":" + seconds);
        if (timer === 0) {
            clearInterval(iiid);
            showExitModal(1);
           
          
        } else {
            timer--;
        }
    }, 1000);
}

